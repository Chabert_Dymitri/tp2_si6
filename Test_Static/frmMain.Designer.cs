﻿namespace Test_Static
{
    partial class frmMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbEntier = new System.Windows.Forms.TextBox();
            this.btnInstancie = new System.Windows.Forms.Button();
            this.lblAffiche = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbEntier
            // 
            this.tbEntier.Location = new System.Drawing.Point(14, 45);
            this.tbEntier.Name = "tbEntier";
            this.tbEntier.Size = new System.Drawing.Size(119, 20);
            this.tbEntier.TabIndex = 0;
            // 
            // btnInstancie
            // 
            this.btnInstancie.Location = new System.Drawing.Point(13, 86);
            this.btnInstancie.Name = "btnInstancie";
            this.btnInstancie.Size = new System.Drawing.Size(70, 35);
            this.btnInstancie.TabIndex = 1;
            this.btnInstancie.Text = "Instancier";
            this.btnInstancie.UseVisualStyleBackColor = true;
            this.btnInstancie.Click += new System.EventHandler(this.btnInstancie_Click);
            // 
            // lblAffiche
            // 
            this.lblAffiche.AutoSize = true;
            this.lblAffiche.Location = new System.Drawing.Point(18, 154);
            this.lblAffiche.Name = "lblAffiche";
            this.lblAffiche.Size = new System.Drawing.Size(0, 13);
            this.lblAffiche.TabIndex = 2;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 261);
            this.Controls.Add(this.lblAffiche);
            this.Controls.Add(this.btnInstancie);
            this.Controls.Add(this.tbEntier);
            this.Name = "frmMain";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbEntier;
        private System.Windows.Forms.Button btnInstancie;
        private System.Windows.Forms.Label lblAffiche;
    }
}

