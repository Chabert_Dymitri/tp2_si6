﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test_Static
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnInstancie_Click(object sender, EventArgs e)
        {
            int i = 0;
            test_static otest = null;
            while (i < Convert.ToInt32(tbEntier.Text))
            {
                otest = new test_static();
                test_static.nb_instanciation++;
                i++;
            }

            lblAffiche.Text = "Le nombre d'intanciation est : " + test_static.nb_instanciation;

        }
    }
}
